<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;




/**
 * @ORM\Entity(repositoryClass="App\Repository\QuestionnaireRepository")
 * @ApiResource(
 *      normalizationContext={
 *          "groups"={"lireQuestionnaire", "créerFormulaireClient"}
 *      }
 *     
 * )
 */
class Questionnaire
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @groups({"lireQuestionnaire"})
     */
    private $id;

    /**
     * @Assert\NotBlank(
     *      message="Le champ nom du questionnaire est vide !"
     * )
     * @ORM\Column(type="string", length=255)
     * @groups({"lireQuestionnaire", "créerFormulaireClient"})
     */
    private $nomQuestionnaire;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Question", mappedBy="questionnaire")
     * @groups({"créerFormulaireClient"})
     */
    private $questions;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Region", mappedBy="questionnaire")
     * @groups({"créerFormulaireClient"})
     */
    private $regions;

    public function __construct()
    {
        $this->questions = new ArrayCollection();
        $this->regions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomQuestionnaire(): ?string
    {
        return $this->nomQuestionnaire;
    }

    public function setNomQuestionnaire(string $nomQuestionnaire): self
    {
        $this->nomQuestionnaire = $nomQuestionnaire;

        return $this;
    }

    /**
     * @return Collection|Question[]
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    public function addQuestion(Question $question): self
    {
        if (!$this->questions->contains($question)) {
            $this->questions[] = $question;
            $question->setQuestionnaire($this);
        }

        return $this;
    }

    public function removeQuestion(Question $question): self
    {
        if ($this->questions->contains($question)) {
            $this->questions->removeElement($question);
            // set the owning side to null (unless already changed)
            if ($question->getQuestionnaire() === $this) {
                $question->setQuestionnaire(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Region[]
     */
    public function getRegions(): Collection
    {
        return $this->regions;
    }

    public function addRegion(Region $region): self
    {
        if (!$this->regions->contains($region)) {
            $this->regions[] = $region;
            $region->addQuestionnaire($this);
        }

        return $this;
    }

    public function removeRegion(Region $region): self
    {
        if ($this->regions->contains($region)) {
            $this->regions->removeElement($region);
            $region->removeQuestionnaire($this);
        }

        return $this;
    }
}
