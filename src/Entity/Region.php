<?php

namespace App\Entity;

use App\Entity\Image;
use App\Entity\Questionnaire;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

use Symfony\Component\Validator\Constraints as Assert;



/**
 * @ORM\Entity(repositoryClass="App\Repository\RegionRepository")
 * @ApiResource(
 *      normalizationContext={
 *          "groups"={"lireRegions"},
 *      }
 * )
 * 
 */


class Region
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @groups({"lireRegions"})
     */
    private $id;

    /**
     * @Assert\NotBlank(
     *      message="Le champ nom de region est vide !"
     * )
     * @ORM\Column(type="string", length=255)
     * @groups({"lireRegions","créerFormulaireClient"})
     * 
     */
    private $nomRegion;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Questionnaire", inversedBy="regions")
     */
    private $questionnaire;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Image", inversedBy="region", cascade={"persist", "remove"})
     * @groups({"créerFormulaireClient"})
     */
    private $image;

    public function __construct()
    {
        $this->questionnaire = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomRegion(): ?string
    {
        return $this->nomRegion;
    }

    public function setNomRegion(string $nomRegion): self
    {
        $this->nomRegion = $nomRegion;

        return $this;
    }

    /**
     * @return Collection|Questionnaire[]
     */
    public function getQuestionnaire(): Collection
    {
        return $this->questionnaire;
    }

    public function addQuestionnaire(Questionnaire $questionnaire): self
    {
        if (!$this->questionnaire->contains($questionnaire)) {
            $this->questionnaire[] = $questionnaire;
        }

        return $this;
    }

    public function removeQuestionnaire(Questionnaire $questionnaire): self
    {
        if ($this->questionnaire->contains($questionnaire)) {
            $this->questionnaire->removeElement($questionnaire);
        }

        return $this;
    }
    /**
     * Undocumented function
     *
     * @return Image|null
     */
    public function getImage(): ?Image
    {
        return $this->image;
    }
    /**
     * Undocumented function
     *
     * @param Image|null $image
     * @return self
     */
    public function setImage(?Image $image): self
    {
        $this->image = $image;

        return $this;
    }
}
