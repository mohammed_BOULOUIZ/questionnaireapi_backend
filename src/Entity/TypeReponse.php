<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\Common\Collections\ArrayCollection;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\TypeReponseRepository")
 * @ApiResource(
 *      normalizationContext={
 *          "groups"={"lireType"}
 *      }
 * )
 */
class TypeReponse
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @groups({"lireType"})
     */
    private $id;

    /**
     * @Assert\NotBlank(
     *       message="Le champ type de reponse est vide !"
     * )
     * @ORM\Column(type="string", length=255)
     * @groups({"lireType","créerFormulaireClient"})
     */
    private $nomTypeReponse;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Question", mappedBy="typereponse")
     */
    private $questions;

    public function __construct()
    {
        $this->questions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomTypeReponse(): ?string
    {
        return $this->nomTypeReponse;
    }

    public function setNomTypeReponse(string $nomTypeReponse): self
    {
        $this->nomTypeReponse = $nomTypeReponse;

        return $this;
    }

    /**
     * @return Collection|Question[]
     */
    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    public function addQuestion(Question $question): self
    {
        if (!$this->questions->contains($question)) {
            $this->questions[] = $question;
            $question->addTypereponse($this);
        }

        return $this;
    }

    public function removeQuestion(Question $question): self
    {
        if ($this->questions->contains($question)) {
            $this->questions->removeElement($question);
            $question->removeTypereponse($this);
        }

        return $this;
    }
}
