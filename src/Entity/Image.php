<?php

namespace App\Entity;

use App\Entity\Region;

use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 * @ApiResource(
 *      normalizationContext={
 *          "groups"={"lireImages"}
 *      }
 * )
*/


class Image
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @groups({"lireImages"})
     */
    private $id;

    /**
     * @Assert\NotBlank(
     *      message="Le champ nom image est vide !"
     * )
     * @ORM\Column(type="string", length=255)
     * @groups({"lireImages"})
     */
    private $nomImage;

    /**
     * @Assert\NotBlank(
     *      message="Le champ url image est vide !"
     * )
     * @ORM\Column(type="string", length=400)
     * @groups({"lireImages","créerFormulaireClient"})
     */
    private $urlImage;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Region", mappedBy="image", cascade={"persist", "remove"})
     */
    private $region;

    /**
     * Undocumented function
     *
     * @return integer|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getNomImage(): ?string
    {
        return $this->nomImage;
    }

    /**
     * Undocumented function
     *
     * @param string $nomImage
     * @return self
     */
    public function setNomImage(string $nomImage): self
    {
        $this->nomImage = $nomImage;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return string|null
     */
    public function getUrlImage(): ?string
    {
        return $this->urlImage;
    }

    /**
     * Undocumented function
     *
     * @param string $urlImage
     * @return self
     */
    public function setUrlImage(string $urlImage): self
    {
        $this->urlImage = $urlImage;

        return $this;
    }

    /**
     * Undocumented function
     *
     * @return Region|null
     */
    public function getRegion(): ?Region
    {
        return $this->region;
    }

    /**
     * Undocumented function
     *
     * @param Region|null $region
     * @return self
     */
    public function setRegion(?Region $region): self
    {
        $this->region = $region;

        // set (or unset) the owning side of the relation if necessary
        $newImage = $region === null ? null : $this;
        if ($newImage !== $region->getImage()) {
            $region->setImage($newImage);
        }

        return $this;
    }
}
