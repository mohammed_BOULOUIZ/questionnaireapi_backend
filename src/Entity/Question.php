<?php

namespace App\Entity;

use App\Entity\TypeReponse;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;



/**
 * @ORM\Entity(repositoryClass="App\Repository\QuestionRepository")
 * @ApiResource(
 *      normalizationContext={"groups"={"lireQuestion"}}
 * )
 */
class Question
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @groups({"lireQuestion","créerFormulaireClient"})
     */
    private $id;

    /**
     * @Assert\NotBlank(
     *      message="Le champ contenu est vide !"
     * )
     * @ORM\Column(type="text")
     * @groups({"lireQuestion","créerFormulaireClient"})
     */
    private $contenu;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ThemeQuestion", inversedBy="questions")
     * @ORM\JoinTable(name="question_type_reponse")
     * @ORM\JoinColumn(nullable=false)
     * @groups({"lireQuestion","créerFormulaireClient"})
     */
    private $themequestion;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\TypeReponse", inversedBy="questions")
     * 
     * @ORM\JoinColumn(nullable=false)
     * @groups({"lireQuestion","créerFormulaireClient"})
     */
    private $typereponse;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Questionnaire", inversedBy="questions")
     * @ORM\JoinColumn(nullable=false)
     * @groups({"lireQuestion","créerFormulaireClient"})
     */
    private $questionnaire;

    public function __construct()
    {
        $this->typereponse = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContenu(): ?string
    {
        return $this->contenu;
    }

    public function setContenu(string $contenu): self
    {
        $this->contenu = $contenu;

        return $this;
    }

    public function getThemequestion(): ?ThemeQuestion
    {
        return $this->themequestion;
    }

    public function setThemequestion(?ThemeQuestion $themequestion): self
    {
        $this->themequestion = $themequestion;

        return $this;
    }

    /**
     * @return Collection|TypeReponse[]
     * @groups({"lireQuestion"})
     */
    public function getTypereponse(): Collection
    {
        return $this->typereponse;
    }

    public function addTypereponse(TypeReponse $typereponse): self
    {
        if (!$this->typereponse->contains($typereponse)) {
            $this->typereponse[] = $typereponse;
        }

        return $this;
    }

    public function removeTypereponse(TypeReponse $typereponse): self
    {
        if ($this->typereponse->contains($typereponse)) {
            $this->typereponse->removeElement($typereponse);
        }

        return $this;
    }

    public function getQuestionnaire(): ?Questionnaire
    {
        return $this->questionnaire;
    }

    public function setQuestionnaire(?Questionnaire $questionnaire): self
    {
        $this->questionnaire = $questionnaire;

        return $this;
    }
}
