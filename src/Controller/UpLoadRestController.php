<?php

namespace App\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;
use Gedmo\Sluggable\Util\Urlizer;

class UpLoadRestController extends FOSRestController
{
    
    public function upLoad(Request $request)
    {    
        /****************chercher le data depuis le append dans React & definir le repertoire de destination */
        $uploadedFile = $request->files->get('file');
        $destination = $this->getParameter('images_directory'); 
        /**************************************************************************************************** */

        /****************Recuperer le nom original du fichier *********************************************** */
        $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
        /**************************************************************************************************** */

        /****************controller le nom du fichier afin d'eviter les cartéres (espce, tab....) *********** */
        // composer require gedmo/doctrine-extensions
        $newFilename = Urlizer::urlize($originalFilename) . $uploadedFile->guessExtension();
        /**************************************************************************************************** */

        /****************envoi du fichier comme l'originale et controlé ************************************* */
        $uploadedFile->move( $destination, $newFilename);
        /**************************************************************************************************** */
    }
}
