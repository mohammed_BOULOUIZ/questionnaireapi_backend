<?php

namespace App\Controller;

use App\Entity\Image;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;

class ImageRestController extends FOSRestController
{

    public function ajouterImage(Request $request)
    {
        /****************Recuperation des données (JSON) depuis la requete*******************************************/
        $jsonData = $request->getContent();

        /****************Deserialize les données en string pour ecréture dans la base********************************/
        $iamgeDesz = $this->get('serializer')->deserialize($jsonData, Image::class, 'json');

        /****************Controler les données deserializer en fonction des contrainte imposées dans les classes*****/
        $errors = $this->get('validator')->validate($iamgeDesz);
        if (count($errors)) {
            return $this->view($errors, Response::HTTP_PARTIAL_CONTENT);
        } else {

        /****************Persister les données (valide) dans la base**************************************************/
            $em = $this->getDoctrine()->getManager();
            $em->persist($iamgeDesz);
            $em->flush();
            
        /****************Renvoyer la reponse************************************************************************/
            return  $this->view($iamgeDesz, Response::HTTP_CREATED);
        }
    }


    public function listImages()
    {
        /****************Recuperation des données  depuis la base******************************************************/
        $repo = $this->getDoctrine()->getRepository(Image::class);
        $images = $repo->findAll();

        /****************Serializer les données en JSON****************************************************************/
        $imageJson = $this->get('serializer')->serialize($images, 'json', ['groups' => 'lireImages']);

        /****************Retourner les données en JSON******************************************************************/
        $serzImages = new Response($imageJson);
         //$token = bin2hex(random_bytes(32));
        return $serzImages;
    }


    public function supprimerImage(Request $request)
    {
        /****************Recuperation l'ID envoyé dans la requete depuis le client***************************************/
        $id = $request->get('id');

        /****************Recuperation l'objet identifié par l'ID*********************************************************/
        $em = $this->getDoctrine()->getManager();
        $repo = $this->getDoctrine()->getRepository(Image::class);
        $deletedIamge = $repo->find($id);

        /****************Supprimer l'Objet en question********************************************************************/
        if ($deletedIamge) {
            $em->remove($deletedIamge);
            $em->flush();
            return $this->handleView($this->view($deletedIamge, Response::HTTP_OK));
        }else{
            return $this->handleView($this->view($deletedIamge, Response::HTTP_NOT_FOUND));
        }
    }


    public function modifierImage(Request $request)
    {
        /****************Recuperer l'ID et le nouvel objet envoyé dans la requete depuis le client****************************/
        $id = $request->get('id');
        $jsonData = $request->getContent();

        /****************Avec le nouvel objet ->le deserializer et extraire ses attributs*************************************/
        $iamgeDesz = new Image(); // créer un nouvel objet
        $iamgeDesz = $this->get('serializer')->deserialize($jsonData, Image::class, 'json'); //lui attibuer les donnée de la requete deserializer
        $nwNomImage = $iamgeDesz->getNomImage(); //recuperer l'attribut
        $nwUrlIamge = $iamgeDesz->getUrlImage(); //recuperer l'attribut

        /****************Avec l'ID -> recuperer l'objet à à modifier***********************************************************/
        $em = $this->getDoctrine()->getManager();
        $repo = $this->getDoctrine()->getRepository(Image::class);
        $updatedImage = $repo->find($id);

        /****************Point de controle : si l'ancien objet existe, on lui SET les nouvels attributs et on update************/
        if ($updatedImage) {
            $updatedImage->setNomImage($nwNomImage);
            $updatedImage->setUrlImage($nwUrlIamge);
            $em->merge($updatedImage);
            $em->flush();
            return $this->handleView($this->view($updatedImage, Response::HTTP_OK));
        }else{
            return $this->handleView($this->view($updatedImage, Response::HTTP_NOT_FOUND));
        }

        /****************si l'ancien objet n'existe pas, on renvoie une reponse HTTP_NOT_FOUND**********************************/
    }

    public function trouverImage(Request $request)
    {
        /****************Recuperer l'ID dans la requete depuis le client********************************************************/
        $id = $request->get('id');
    
        /****************Avec l'ID -> recuperer l'objet demandé*****************************************************************/
        $repo = $this->getDoctrine()->getRepository(Image::class);
        $image = $repo->find($id);
    
        /****************Serializer l'objet*************************************************************************************/
        $imageJson = $this->get('serializer')->serialize($image, 'json', ['groups' => 'lireImages']);

        /****************Retourner l'objet en reponse***************************************************************************/
        $serzImage = new Response($imageJson);
        return $serzImage;
    }
}
