<?php

namespace App\Controller;

use App\Entity\ThemeQuestion;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;

class ThemeQuestionRestController extends FOSRestController
{
    public function ajouterTheme(Request $request)
    {
        $jsonData = $request->getContent();
        $ThemeDesz = $this->get('serializer')->deserialize($jsonData, ThemeQuestion::class, 'json');

        $errors = $this->get('validator')->validate($ThemeDesz);
        if (count($errors)) {

            return $this->view($errors, Response::HTTP_BAD_REQUEST);
        } else {

        $em = $this->getDoctrine()->getManager();
        $em->persist($ThemeDesz);
        $em->flush();
        return  $this->view($ThemeDesz, Response::HTTP_CREATED);
        }
    }

    public function listThemes()
    {
        $repo = $this->getDoctrine()->getRepository(ThemeQuestion::class);
        $Theme = $repo->findAll();
        $Themejson = $this->get('serializer')->serialize($Theme, 'json', ['groups' => 'lireTheme']);

        $serzThemes = new Response($Themejson);

        return $serzThemes;
    }


    public function supprimerTheme(Request $request)
    {
        $id = $request->get('id');

        $em = $this->getDoctrine()->getManager();
        $repo = $this->getDoctrine()->getRepository(ThemeQuestion::class);
        $deletedTheme = $repo->find($id);

        $em->remove($deletedTheme);
        $em->flush();
    }


    public function modifierTheme(Request $request)
    {
        $id = $request->get('id');
        $jsonData = $request->getContent();

        $ThemeDesz = new ThemeQuestion();
        $ThemeDesz = $this->get('serializer')->deserialize($jsonData, ThemeQuestion::class, 'json');
        $nwNomTheme = $ThemeDesz->getNomTheme();

        $em = $this->getDoctrine()->getManager();
        $repo = $this->getDoctrine()->getRepository(ThemeQuestion::class);
        $updatedTheme = $repo->find($id);

        if ($updatedTheme) {
            $updatedTheme->setNomTheme($nwNomTheme);
            $em->merge($updatedTheme);
            $em->flush();
            return $this->handleView($this->view($updatedTheme, Response::HTTP_OK));
        }
        return $this->handleView($this->view($updatedTheme, Response::HTTP_NOT_FOUND));
    }

    
    public function trouverTheme(Request $request)
    {
        /****************Recuperer l'ID dans la requete depuis le client********************************************************/
        $id = $request->get('id');
    
        /****************Avec l'ID -> recuperer l'objet demandé*****************************************************************/
        $repo = $this->getDoctrine()->getRepository(ThemeQuestion::class);
        $theme = $repo->find($id);
    
        /****************Serializer l'objet*************************************************************************************/
        $themeJson = $this->get('serializer')->serialize($theme, 'json', ['groups' => 'lireTheme']);

        /****************Retourner l'objet en reponse***************************************************************************/
        $serzTheme = new Response($themeJson);
        return $serzTheme;
    }
}
