<?php

namespace App\Controller;

use App\Entity\Region;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;



class RegionRestController extends FOSRestController
{

    public function ajouterRegion(Request $request)
    {
        $jsonData = $request->getContent();
        $regionDesz = $this->get('serializer')->deserialize($jsonData, Region::class, 'json');

        $errors = $this->get('validator')->validate($regionDesz);
        if (count($errors)) {
            return $this->view($errors, Response::HTTP_BAD_REQUEST);

        } else {
        $em = $this->getDoctrine()->getManager();
        $em->persist($regionDesz);
        $em->flush();
        return  $this->view($regionDesz, Response::HTTP_CREATED);
        }
    }

    public function listRegions()
    {
        $repo = $this->getDoctrine()->getRepository(Region::class);
        $region = $repo->findAll();
        $Regionjson = $this->get('serializer')->serialize($region, 'json', ['groups' => 'lireRegions']);

        $serzRegions = new Response($Regionjson);

        return $serzRegions;
    }


    public function supprimerRegion(Request $request)
    {
        $id = $request->get('id');

        $em = $this->getDoctrine()->getManager();
        $repo = $this->getDoctrine()->getRepository(Region::class);
        $deletedRegion = $repo->find($id);
        $em->remove($deletedRegion);
        $em->flush();
    }


    public function modifierRegion(Request $request)
    {
        $id = $request->get('id');
        $jsonData = $request->getContent();

        $RegionDesz = new Region();
        $RegionDesz = $this->get('serializer')->deserialize($jsonData, Region::class, 'json');
        $nwNomRegion = $RegionDesz->getNomRegion();

        $em = $this->getDoctrine()->getManager();
        $repo = $this->getDoctrine()->getRepository(Region::class);
        $updatedRegion = $repo->find($id);

        if ($updatedRegion) {
            $updatedRegion->setNomRegion($nwNomRegion);
            $em->merge($updatedRegion);
            $em->flush();
            return $this->handleView($this->view($updatedRegion, Response::HTTP_OK));
        }
        return $this->handleView($this->view($updatedRegion, Response::HTTP_NOT_FOUND));
    }

    public function trouverRegion(Request $request)
    {
        /****************Recuperer l'ID dans la requete depuis le client********************************************************/
        $id = $request->get('id');
    
        /****************Avec l'ID -> recuperer l'objet demandé*****************************************************************/
        $repo = $this->getDoctrine()->getRepository(Region::class);
        $region = $repo->find($id);
    
        /****************Serializer l'objet*************************************************************************************/
        $regionJson = $this->get('serializer')->serialize($region, 'json', ['groups' => 'lireRegions']);

        /****************Retourner l'objet en reponse***************************************************************************/
        $serzRegion = new Response($regionJson);
        return $serzRegion;
    }
}