<?php

namespace App\Controller;

use App\Entity\Question;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;

class QuestionRestController extends FOSRestController
{
    public function ajouterQuestion(Request $request)
    {
        $jsonData = $request->getContent();

        $questionDesz = $this->get('serializer')->deserialize($jsonData, Question::class, 'json');

        $errors = $this->get('validator')->validate($questionDesz);
        if (count($errors)) {
            return $this->view($errors, Response::HTTP_BAD_REQUEST);
        } else {
            $em = $this->getDoctrine()->getManager();
            $em->persist($questionDesz);
            $em->flush();
            return  $this->view($questionDesz, Response::HTTP_CREATED);
        }
    }


    public function listQuestions()
    {
        $repo = $this->getDoctrine()->getRepository(Question::class);
        $Questions = $repo->findAll();

        $Questionjson = $this->get('serializer')->serialize($Questions, 'json', ['groups' => 'lireQuestion']);

        $serzQuestions = new Response($Questionjson);

        return $serzQuestions;
    }


    public function supprimerQuestion(Request $request)
    {
        $id = $request->get('id');

        $em = $this->getDoctrine()->getManager();
        $repo = $this->getDoctrine()->getRepository(Question::class);
        $deletedQuestion = $repo->find($id);

        $em->remove($deletedQuestion);
        $em->flush();
    }


    public function modifierQuestion(Request $request)
    {
        $id = $request->get('id');
        $jsonData = $request->getContent();

        $questionDesz = $this->get('serializer')->deserialize($jsonData, Question::class, 'json');
        $nwContenuQuestion = $questionDesz->getcontenu();
        $nwQuestionnaire = $questionDesz->getQuestionnaire();
        $nwThemequestion = $questionDesz->getThemequestion();
        $nwTypereponses = $questionDesz->getTypereponse();

        $em = $this->getDoctrine()->getManager();
        $repo = $this->getDoctrine()->getRepository(Question::class);

        $updatedQuestion = $repo->find($id);

        if ($updatedQuestion) {
            $updatedQuestion->setcontenu($nwContenuQuestion);
            $updatedQuestion->setQuestionnaire($nwQuestionnaire);
            $updatedQuestion->setThemequestion($nwThemequestion);


            $updateTypes = $updatedQuestion->getTypereponse();
            if ($updateTypes) {
                foreach ($updateTypes as $updateType) {
                    $updatedQuestion->removeTypereponse($updateType);
                }
            }

            foreach ($nwTypereponses as $nwTypereponse) {

                $updatedQuestion->addTypereponse($nwTypereponse);
            }

            $em->flush();
            return $this->handleView($this->view($updatedQuestion, Response::HTTP_OK));
        }
        return $this->handleView($this->view($questionDesz, Response::HTTP_NOT_FOUND));
    }

    public function trouverQuestion(Request $request)
    {
        /****************Recuperer l'ID dans la requete depuis le client********************************************************/
        $id = $request->get('id');

        /****************Avec l'ID -> recuperer l'objet demandé*****************************************************************/
        $repo = $this->getDoctrine()->getRepository(Question::class);
        $question = $repo->find($id);

        /****************Serializer l'objet*************************************************************************************/
        $questionJson = $this->get('serializer')->serialize($question, 'json', ['groups' => 'lireQuestion']);

        /****************Retourner l'objet en reponse***************************************************************************/
        $serzQuestion = new Response($questionJson);
        return $serzQuestion;
    }
}
