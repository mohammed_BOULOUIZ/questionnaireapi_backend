<?php

namespace App\Controller;

use App\Entity\Questionnaire;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;;

class QuestionnaireRestController extends FOSRestController
{
    public function ajouterQuestionnaire(Request $request)
    {
        $jsonData = $request->getContent();
        $questionnaireDesz = $this->get('serializer')->deserialize($jsonData, Questionnaire::class, 'json');

        $errors = $this->get('validator')->validate($questionnaireDesz);
        if (count($errors)) {
            return $this->view($errors, Response::HTTP_BAD_REQUEST);

        } else {

        $em = $this->getDoctrine()->getManager();
        $em->persist($questionnaireDesz);
        $em->flush();

        return  $this->view($questionnaireDesz, Response::HTTP_CREATED);
        }
    }


    public function listQuestionnaires()
    {
        $repo = $this->getDoctrine()->getRepository(Questionnaire::class);
        $Questionnaires = $repo->findAll();
        $Questionnairejson = $this->get('serializer')->serialize($Questionnaires, 'json', ['groups' => 'lireQuestionnaire']);

        $serzQuestionnaires = new Response($Questionnairejson);

        return $serzQuestionnaires;
    }


    public function supprimerQuestionnaire(Request $request)
    {
        $id = $request->get('id');

        $em = $this->getDoctrine()->getManager();
        $repo = $this->getDoctrine()->getRepository(Questionnaire::class);
        $deletedIamge = $repo->find($id);

        $em->remove($deletedIamge);
        $em->flush();
    }


    public function modifierQuestionnaire(Request $request)
    {
        $id = $request->get('id');
        $jsonData = $request->getContent();

        $iamgeDesz = new Questionnaire();
        $iamgeDesz = $this->get('serializer')->deserialize($jsonData, Questionnaire::class, 'json');
        $nwNomQuestionnaire = $iamgeDesz->getNomQuestionnaire();

        $em = $this->getDoctrine()->getManager();
        $repo = $this->getDoctrine()->getRepository(Questionnaire::class);
        $updatedQuestionnaire = $repo->find($id);

        if ($updatedQuestionnaire) {
            $updatedQuestionnaire->setNomQuestionnaire($nwNomQuestionnaire);
            $em->merge($updatedQuestionnaire);
            $em->flush();
            return $this->handleView($this->view($updatedQuestionnaire, Response::HTTP_OK));
        }
        return $this->handleView($this->view($updatedQuestionnaire, Response::HTTP_NOT_FOUND));
    }

    public function trouverQuestionnaire(Request $request)
    {
        /****************Recuperer l'ID dans la requete depuis le client********************************************************/
        $id = $request->get('id');
    
        /****************Avec l'ID -> recuperer l'objet demandé*****************************************************************/
        $repo = $this->getDoctrine()->getRepository(Questionnaire::class);
        $questionnaire = $repo->find($id);
    
        /****************Serializer l'objet*************************************************************************************/
        $questionnaireJson = $this->get('serializer')->serialize($questionnaire, 'json', ['groups' => 'lireQuestionnaire']);

        /****************Retourner l'objet en reponse***************************************************************************/
        $serzQuestionnaire = new Response($questionnaireJson);
        return $serzQuestionnaire;
    }
}
