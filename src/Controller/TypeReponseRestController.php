<?php

namespace App\Controller;

use App\Entity\TypeReponse;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\FOSRestController;

class TypeReponseRestController extends FOSRestController
{
    public function ajouterTypeReponse(Request $request)
    {
        $jsonData = $request->getContent();
        $typeDesz = $this->get('serializer')->deserialize($jsonData, TypeReponse::class, 'json');
        
        $errors = $this->get('validator')->validate($typeDesz);
        if (count($errors)) {
            return $this->view($errors, Response::HTTP_PARTIAL_CONTENT);

        } else {
        $em = $this->getDoctrine()->getManager();
        $em->persist($typeDesz);
        $em->flush();

        return  $this->view($typeDesz, Response::HTTP_CREATED);
        }
    }

    public function listTypeReponses()
    {
        $repo = $this->getDoctrine()->getRepository(TypeReponse::class);
        $Type = $repo->findAll();
        $Typejson = $this->get('serializer')->serialize($Type, 'json', ['groups' => 'lireType']);

        $serzTypes = new Response($Typejson);

        return $serzTypes;
    }


    public function supprimerTypeReponse(Request $request)
    {
        $id = $request->get('id');

        $em = $this->getDoctrine()->getManager();
        $repo = $this->getDoctrine()->getRepository(TypeReponse::class);
        $deletedType = $repo->find($id);

        $em->remove($deletedType);
        $em->flush();
    }


    public function modifierTypeReponse(Request $request)
    {
        $id = $request->get('id');
        $jsonData = $request->getContent();

        $TypeDesz = new TypeReponse();
        $TypeDesz = $this->get('serializer')->deserialize($jsonData, TypeReponse::class, 'json');
        $nwNomType = $TypeDesz->getNomTypeReponse();

        $em = $this->getDoctrine()->getManager();
        $repo = $this->getDoctrine()->getRepository(TypeReponse::class);
        $updatedType = $repo->find($id);

        if ($updatedType) {
            $updatedType->setNomTypeReponse($nwNomType);
            $em->merge($updatedType);
            $em->flush();
            return $this->handleView($this->view($updatedType, Response::HTTP_OK));
        }
        return $this->handleView($this->view($updatedType, Response::HTTP_NOT_FOUND));
    }
    
    public function trouverTypeReponse(Request $request)
    {
        /****************Recuperer l'ID dans la requete depuis le client********************************************************/
        $id = $request->get('id');
    
        /****************Avec l'ID -> recuperer l'objet demandé*****************************************************************/
        $repo = $this->getDoctrine()->getRepository(TypeReponse::class);
        $typeReponse = $repo->find($id);
    
        /****************Serializer l'objet*************************************************************************************/
        $typeReponseJson = $this->get('serializer')->serialize($typeReponse, 'json', ['groups' => 'lireType']);

        /****************Retourner l'objet en reponse***************************************************************************/
        $serzTypeReponse = new Response($typeReponseJson);
        return $serzTypeReponse;
    }
}
