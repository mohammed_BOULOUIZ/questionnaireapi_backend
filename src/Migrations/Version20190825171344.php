<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190825171344 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE TABLE image (id INT IDENTITY NOT NULL, nom_image NVARCHAR(255) NOT NULL, url_image NVARCHAR(400) NOT NULL, PRIMARY KEY (id))');
        $this->addSql('CREATE TABLE question (id INT IDENTITY NOT NULL, themequestion_id INT NOT NULL, questionnaire_id INT NOT NULL, contenu VARCHAR(MAX) NOT NULL, PRIMARY KEY (id))');
        $this->addSql('CREATE INDEX IDX_B6F7494EEEAB3A80 ON question (themequestion_id)');
        $this->addSql('CREATE INDEX IDX_B6F7494ECE07E8FF ON question (questionnaire_id)');
        $this->addSql('CREATE TABLE question_type_reponse (question_id INT NOT NULL, type_reponse_id INT NOT NULL, PRIMARY KEY (question_id, type_reponse_id))');
        $this->addSql('CREATE INDEX IDX_E47D4A701E27F6BF ON question_type_reponse (question_id)');
        $this->addSql('CREATE INDEX IDX_E47D4A70D21B6DA7 ON question_type_reponse (type_reponse_id)');
        $this->addSql('CREATE TABLE questionnaire (id INT IDENTITY NOT NULL, nom_questionnaire NVARCHAR(255) NOT NULL, PRIMARY KEY (id))');
        $this->addSql('CREATE TABLE region (id INT IDENTITY NOT NULL, image_id INT, nom_region NVARCHAR(255) NOT NULL, PRIMARY KEY (id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F62F1763DA5256D ON region (image_id) WHERE image_id IS NOT NULL');
        $this->addSql('CREATE TABLE region_questionnaire (region_id INT NOT NULL, questionnaire_id INT NOT NULL, PRIMARY KEY (region_id, questionnaire_id))');
        $this->addSql('CREATE INDEX IDX_2AC2733598260155 ON region_questionnaire (region_id)');
        $this->addSql('CREATE INDEX IDX_2AC27335CE07E8FF ON region_questionnaire (questionnaire_id)');
        $this->addSql('CREATE TABLE theme_question (id INT IDENTITY NOT NULL, nom_theme NVARCHAR(255) NOT NULL, PRIMARY KEY (id))');
        $this->addSql('CREATE TABLE type_reponse (id INT IDENTITY NOT NULL, nom_type_reponse NVARCHAR(255) NOT NULL, PRIMARY KEY (id))');
        $this->addSql('ALTER TABLE question ADD CONSTRAINT FK_B6F7494EEEAB3A80 FOREIGN KEY (themequestion_id) REFERENCES theme_question (id)');
        $this->addSql('ALTER TABLE question ADD CONSTRAINT FK_B6F7494ECE07E8FF FOREIGN KEY (questionnaire_id) REFERENCES questionnaire (id)');
        $this->addSql('ALTER TABLE question_type_reponse ADD CONSTRAINT FK_E47D4A701E27F6BF FOREIGN KEY (question_id) REFERENCES question (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE question_type_reponse ADD CONSTRAINT FK_E47D4A70D21B6DA7 FOREIGN KEY (type_reponse_id) REFERENCES type_reponse (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE region ADD CONSTRAINT FK_F62F1763DA5256D FOREIGN KEY (image_id) REFERENCES image (id)');
        $this->addSql('ALTER TABLE region_questionnaire ADD CONSTRAINT FK_2AC2733598260155 FOREIGN KEY (region_id) REFERENCES region (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE region_questionnaire ADD CONSTRAINT FK_2AC27335CE07E8FF FOREIGN KEY (questionnaire_id) REFERENCES questionnaire (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mssql', 'Migration can only be executed safely on \'mssql\'.');

        $this->addSql('CREATE SCHEMA db_accessadmin');
        $this->addSql('CREATE SCHEMA db_backupoperator');
        $this->addSql('CREATE SCHEMA db_datareader');
        $this->addSql('CREATE SCHEMA db_datawriter');
        $this->addSql('CREATE SCHEMA db_ddladmin');
        $this->addSql('CREATE SCHEMA db_denydatareader');
        $this->addSql('CREATE SCHEMA db_denydatawriter');
        $this->addSql('CREATE SCHEMA db_owner');
        $this->addSql('CREATE SCHEMA db_securityadmin');
        $this->addSql('CREATE SCHEMA dbo');
        $this->addSql('ALTER TABLE region DROP CONSTRAINT FK_F62F1763DA5256D');
        $this->addSql('ALTER TABLE question_type_reponse DROP CONSTRAINT FK_E47D4A701E27F6BF');
        $this->addSql('ALTER TABLE question DROP CONSTRAINT FK_B6F7494ECE07E8FF');
        $this->addSql('ALTER TABLE region_questionnaire DROP CONSTRAINT FK_2AC27335CE07E8FF');
        $this->addSql('ALTER TABLE region_questionnaire DROP CONSTRAINT FK_2AC2733598260155');
        $this->addSql('ALTER TABLE question DROP CONSTRAINT FK_B6F7494EEEAB3A80');
        $this->addSql('ALTER TABLE question_type_reponse DROP CONSTRAINT FK_E47D4A70D21B6DA7');
        $this->addSql('DROP TABLE image');
        $this->addSql('DROP TABLE question');
        $this->addSql('DROP TABLE question_type_reponse');
        $this->addSql('DROP TABLE questionnaire');
        $this->addSql('DROP TABLE region');
        $this->addSql('DROP TABLE region_questionnaire');
        $this->addSql('DROP TABLE theme_question');
        $this->addSql('DROP TABLE type_reponse');
    }
}
